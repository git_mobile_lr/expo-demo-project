import React from 'react'
import { TouchableOpacity, View, Text } from 'react-native'

import { font, button } from '@styles'

const Button = ({ text, color = "yellow", disabled, buttonStyle = {}, onPress }) => {

    return (
        <TouchableOpacity
            onPress={ onPress instanceof Function ? onPress : () => {} }
            disabled={ disabled }
            style={ buttonStyle }
        >
            <View style={[ button[color], button.style, { opacity: disabled ? 0.5 : 1 } ]}>
                <Text style={[ font.color.white, font.weight.bold, font.size.md, font.align.center ]}>
                    { text }
                </Text>
            </View>
        </TouchableOpacity>
    )
}

export { Button }