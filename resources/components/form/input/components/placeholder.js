import React from 'react'
import { Animated, Text } from 'react-native'

import { Icon } from '@components/common' // see icon.js in components/common to see all available icon types
import { flex, font } from '@styles'

const Placeholder = ({
    label,
    styles,
    fadeAnim,
    icon
}) => {
    
    return (
        <Animated.View
            style={[
                styles.labelOverlay,
                flex.direction.row,
                flex.align.center,
                font.size.md,
                {
                    opacity: fadeAnim.interpolate({
                        inputRange: [ 0, 10 ],
                        outputRange: [ 0, 1 ]
                    })
                }
            ]}
        >
            { icon ? <Icon name={ icon.name } type={ icon.type } iconStyle={[ font.size.md, { marginRight: 10 } ]} /> : null }
            <Text>
                { label }
            </Text>
        </Animated.View>
    )
}



export { Placeholder }