import React from 'react'
import { Animated } from 'react-native'
import { font } from '@styles'

const Label = ({
    styles,
    fadeAnim,
    label
}) => {
    
    return (
        <Animated.Text
            style={[
                styles.label,
                font.size.sm,
                {
                    transform: [{ translateY: fadeAnim, }],
                    opacity: fadeAnim.interpolate({
                        inputRange: [ 0, 10 ],
                        outputRange: [ 1, 0 ]
                    })
                }
            ]}>
            { label }
        </Animated.Text>
    )
}



export { Label }