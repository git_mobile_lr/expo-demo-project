import React, { useState, useRef, useEffect } from 'react'
import { Animated, View, TextInput, KeyboardAvoidingView, Platform, TouchableOpacity, Text } from 'react-native'

import { Icon } from '@components/common'
import { Label, Placeholder } from './components'
import styles from './input.styles'
import colors from '@styles/palette.styles'

const Input = ({
    label,
    value,
    name,
    icon,
    onChange,
    containerStyle,
    password,
    hasError = false
}) => {
    const handleChange = (text) => onChange({ name, value: text })
    const [ isFocused, setIsFocused ] = useState(false)
    const [ masked, setMasked ] = useState(password ? password : false )
    const fadeAnim = useRef(new Animated.Value(10)).current;
    const toggleAnimation = (toValue) => {
        Animated.timing(
            fadeAnim,
            {
              toValue,
              duration: 300,
              useNativeDriver: true
            }
          ).start();
    };
    useEffect(() => toggleAnimation(isFocused || value ? 0 : 10) , [isFocused]);
    
    return (
        <KeyboardAvoidingView
            behavior={Platform.OS === "ios" ? "padding" : "height"}
            style={ containerStyle }
            >
            { label ? <Label styles={ styles } fadeAnim={ fadeAnim } label={ label } /> : null }
            <View style={[ styles.inputGroup, { borderColor: hasError ? colors.red : (isFocused ? colors.yellow : "#B8B8B8") } ]}>
                { label ? <Placeholder styles={ styles } fadeAnim={ fadeAnim } label={ label } icon={ icon } /> : null }
                <TextInput
                    value={ value ? value : '' }
                    onChangeText={ handleChange }
                    style={[ styles.input ]}
                    onFocus={ () => setIsFocused(true) }
                    onBlur={ () => !value ? setIsFocused(false) : {} }
                    secureTextEntry={ masked }
                />
                {
                    password ? (
                        <TouchableOpacity onPress={ () => setMasked(prev => !prev) }>
                            <Icon name={`eye${ masked ? '' : '-off' }`} type="Ionicons" size={ 20 } iconStyle={{ color: "#B8B8B8" }} />
                        </TouchableOpacity>
                    ) : null
                }
            </View>
        </KeyboardAvoidingView>
    )
}



export { Input }