import colors from '@styles/palette.styles'
const styles = {
    label: {
        paddingBottom: 10,
        color: colors.yellow
    },
    labelOverlay: {
        position: "absolute",
        width: "100%",
        height: "100%",
        left: 15,
        top: 10
    },
    inputGroup: {
        borderWidth: 1,
        borderRadius: 5,
        paddingHorizontal: 15,
        paddingVertical: 10,
        position: "relative",
        backgroundColor: "#fff",
        flexDirection: "row",
        alignItems: "center"
    },
    input: {
        padding: 0,
        margin: 0,
        fontSize: 13,
        height: "auto",
        flex: 1
    }
}

export default styles