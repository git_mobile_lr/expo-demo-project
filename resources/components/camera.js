import React, { useRef, useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { View, Animated, Dimensions, TouchableOpacity, Text } from 'react-native'
// import { RNCamera } from 'react-native-camera' // TODO CHANGE

import { Icon } from 'components/common'
import { toggleCamera } from 'actions/camera.actions'
import { button, font } from '@styles'
const screenHeight = Dimensions.get("screen").height
const Camera = () => {
    const { inUse, onCapture } = useSelector(state => state.camera)
    const [ uri, setUri ] = useState('')
    const [ base64, setBase64 ] = useState('')
    const cameraRef = useRef()
    const inUseAnimation = useRef(new Animated.Value(0)).current
    const toggleAnimation = (toValue) => {
        Animated.timing(
            inUseAnimation,
            {
              toValue,
              duration: 250,
              useNativeDriver: true
            }
          ).start()
    }
    const handleCapture = async () => {
        if (cameraRef.current) {
            if (uri) {
                if (onCapture && onCapture instanceof Function) onCapture(base64)
                toggleCamera({ inUse: false })
            }
            else {
                const options = { quality: 1, base64: true, pauseAfterCapture: true }
                const data = await cameraRef.current.takePictureAsync(options)
                setUri(data.uri)
                setBase64(data.base64)
            }
        }
    }
    useEffect(() => {
        toggleAnimation(inUse ? 1 : 0)
        if (uri) {
            setUri('')
            cameraRef.current.resumePreview()
        }
    }, [inUse])

    return (
        <Animated.View
            style={[
                styles.camera,
                {
                    zIndex: 1,
                    opacity: inUseAnimation,
                    transform: [
                        {
                            translateY: inUseAnimation.interpolate({
                                inputRange: [ 0, 1 ],
                                outputRange: [ screenHeight, 0 ]
                            })
                        }
                    ]
                }
            ]}>
            {/* <RNCamera
                ref={ cameraRef }
                style={ styles.camera }
                type={RNCamera.Constants.Type.front}
                flashMode={RNCamera.Constants.FlashMode.on}
                androidCameraPermissionOptions={{
                    title: 'Permission to use camera',
                    message: 'We need your permission to use your camera',
                    buttonPositive: 'Ok',
                    buttonNegative: 'Cancel',
                }}
            /> */}
            <View style={{ flex: 1, justifyContent: "flex-end", padding: 20 }}>
                <TouchableOpacity
                    style={[ button.style, button.yellow ]}
                    onPress={ handleCapture }
                >
                    <Text style={[ font.weight.bold, font.color.white, font.align.center ]}>
                        { uri ? "Done" : "Take a Picture" }
                    </Text>
                </TouchableOpacity>
            </View>
            <View style={[ styles.dismissBtn ]}>
                <TouchableOpacity onPress={ () => toggleCamera({ inUse: false }) } style={{ padding: 10 }}>
                    <Icon name="times-circle" size={ 25 } iconStyle={ styles.dismissBtnIcon } />
                </TouchableOpacity>
            </View>
        </Animated.View>
    )
}

const styles = {
    camera: {
        height: "100%",
        width: "100%",
        position: "absolute",
        left: 0,
        top: 0
    },
    dismissBtn: {
        position: "absolute",
        top: 0,
        right: 0,
        zIndex: 1
    },
    dismissBtnIcon: { color: "#fff" }
}

export default Camera