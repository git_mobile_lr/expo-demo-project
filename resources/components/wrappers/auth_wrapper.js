import React from 'react'
import { View, Dimensions, ScrollView } from 'react-native'

const AuthWrapper = ({ children, wrapperStyle }) => {

    return (
        <ScrollView
            automaticallyAdjustContentInsets
        >
            <View style={[ { minHeight: ( Dimensions.get("window").height - 24 ), backgroundColor: "#F8F8F8" }, wrapperStyle ]}>
                { children }
            </View>
        </ScrollView>
    )
}

export default AuthWrapper