import React from 'react'
import { TouchableOpacity } from 'react-native'
import { useNavigation } from '@react-navigation/native'

const Link = ({ to, linkStyle, children }) => {
    const navigation = useNavigation()

    return (
        <TouchableOpacity style={ linkStyle } onPress={ () => navigation.navigate(to) }>
            { children }
        </TouchableOpacity>
    )
}

export { Link }