import React from 'react'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
// import FontAwesome5Pro from 'react-native-vector-icons/FontAwesome5Pro'
import Ionicons from 'react-native-vector-icons/Ionicons'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Octicons from 'react-native-vector-icons/Octicons'
import Feather from 'react-native-vector-icons/Feather'
import AntDesign from 'react-native-vector-icons/AntDesign'

const Icon = ({ name, type, size, iconStyle }) => {

    switch (type) {
        case "FontAwesome": return <FontAwesome name={ name } size={ size } style={ iconStyle } />
        case "FontAwesome5": return <FontAwesome5 name={ name } size={ size } style={ iconStyle } />
        /* `case "FontAwesome5Pro": return <FontAwesome5Pro name={ name } size={ size } style={ iconStyle } />` */
        case "Ionicons": return <Ionicons name={ name } size={ size } style={ iconStyle } />
        case "MaterialIcons": return <MaterialIcons name={ name } size={ size } style={ iconStyle } />
        case "MaterialCommunityIcons": return <MaterialCommunityIcons name={ name } size={ size } style={ iconStyle } />
        case "Octicons": return <Octicons name={ name } size={ size } style={ iconStyle } />
        case "Feather": return <Feather name={ name } size={ size } style={ iconStyle } />
        case "AntDesign": return <AntDesign name={ name } size={ size } style={ iconStyle } />
        default: return <FontAwesome name={ name } size={ size } style={ iconStyle } />
    }
}

export { Icon }