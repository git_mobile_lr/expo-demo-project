import {Dashboard} from '../constants';
import initialState from '../reducers/initialState';

export default function(state = initialState.categories, action) {
  switch (action.type) {
    //LOGIN
    case Dashboard.GET_SERVICE_CATEGORIES:
      return state;
    case Dashboard.GET_SERVICE_CATEGORIES_SUCCESS:
      return action.payload.data;
    case Dashboard.GET_SERVICE_CATEGORIES_FAILED:
      return state;

    default:
      return state;
  }
}
