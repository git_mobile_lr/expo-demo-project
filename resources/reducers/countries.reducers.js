import {Dashboard} from '../constants';
import initialState from '../reducers/initialState';

export default function(state = initialState.countries, action) {
  switch (action.type) {
    //LOGIN
    case Dashboard.GET_COUNTRIES:
      return state;
    case Dashboard.GET_COUNTRIES_SUCCESS:
      return action.payload.data;
    case Dashboard.GET_COUNTRIES_FAILED:
      return state;

    default:
      return state;
  }
}
