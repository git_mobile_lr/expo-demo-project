import {Dashboard} from '../constants';
import initialState from '../reducers/initialState';

export default function(state = initialState.dashboard, action) {
  switch (action.type) {
    case Dashboard.SWITCH_DASHBOARD_TYPE:
      return {
        ...state,
        ...{
          type: action.payload.type
            ? action.payload.type
            : state.type !== 'customer'
            ? 'customer'
            : 'partner',
        },
      };

    default:
      return state;
  }
}
