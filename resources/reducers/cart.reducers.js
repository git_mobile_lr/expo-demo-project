import {Cart} from 'constants';
import initialState from '../reducers/initialState';

export default function(state = initialState.cart, action) {
    let { merchants } = state

    switch (action.type) {
      
        case Cart.UPDATE_CART_MERCHANT:
            merchants[action.payload.id] = action.payload
            if (!merchants[action.payload.id].items || merchants[action.payload.id].items.length === 0) delete merchants[action.payload.id]
            return { ...state, merchants }

        //RETRIEVE CART
        case Cart.GET_CART:
            return state;
        case Cart.GET_CART_SUCCESS:
            return {...state, ...action.payload};
        case Cart.GET_CART_FAILED:
            return {};

        //EMPTY CART
        case Cart.EMPTY_CART:
            return state;

        //ADD TO CART
        case Cart.ADD_TO_CART:
            return state;
        case Cart.ADD_TO_CART_SUCCESS:
            return state;
        case Cart.ADD_TO_CART_FAILED:
            return state;

        default:
            return state;
    }
}
