import { Camera } from '@constants/camera.types';
import initialState from './initialState';

export default function cameraReducer (state = initialState.camera, action) {

    switch(action.type) {
        case Camera.TOGGLE_CAMERA:
            return action.payload;
        default: 
            return state;
    }
}