import {Auth} from '../constants';
import initialState from '../reducers/initialState';

export default function(state = initialState.user, action) {
  switch (action.type) {
    //LOGIN
    case Auth.LOGIN:
      return state;
    case Auth.LOGIN_SUCCESS:
      return {...state, ...action.payload};
    case Auth.LOGIN_FAILED:
      return {};

    //LOGOUT
    case Auth.LOGOUT:
      return initialState.user;

    //SIGNUP
    case Auth.SIGNUP:
      return state;
    case Auth.SIGNUP_SUCCESS:
      return state;
    case Auth.SIGNUP_FAILED:
      return state;

    //GET_USER_DATA
    case Auth.GET_USER_DATA:
      return state;
    case Auth.GET_USER_DATA_SUCCESS:
      return action.payload && action.payload.success
        ? {...state, ...action.payload.data}
        : state;
    case Auth.GET_USER_DATA_FAILED:
      return {};

    //UPDATE_BUSINESS
    case Auth.UPDATE_BUSINESS_NAME:
      return state;
    case Auth.UPDATE_BUSINESS_NAME_SUCCESS:
      return action.payload && action.payload.success
        ? {...state, ...action.payload.data}
        : state;
    case Auth.UPDATE_BUSINESS_NAME_FAILED:
      return state;

    //USER_ADD_SERVICE
    case Auth.USER_ADD_SERVICE:
      return state;
    case Auth.USER_ADD_SERVICE_SUCCESS:
      if (action.payload && action.payload.success) {
        const partner_services = state.partner_services;
        partner_services.push(action.payload.data);
        state.partner_services = partner_services;
      }
      return state;
    case Auth.USER_ADD_SERVICE_FAILED:
      return state;

    //REGISTER_NUMBER
    case Auth.REGISTER_NUMBER:
      return state;
    case Auth.REGISTER_NUMBER_SUCCESS:
      return action.payload && action.payload.success
        ? {...state, ...action.payload.data}
        : state;
    case Auth.REGISTER_NUMBER_FAILED:
      return state;

    //SMS_VERIFY
    case Auth.SMS_VERIFY:
      return state;
    case Auth.SMS_VERIFY_SUCCESS:
      return state;
    case Auth.SMS_VERIFY_FAILED:
      return state;

    //FB_LOGIN
    case Auth.FB_LOGIN:
      return state;
    case Auth.FB_LOGIN_SUCCESS:
      return {...state, ...{token: action.payload.token}};
    case Auth.FB_LOGIN_FAILED:
      return {};

    //PROFILE
    case Auth.UPDATE_PROFILE: {
      return state;
    }
    case Auth.UPDATE_PROFILE_SUCCESS: {
      const data = action.payload.data;
      console.log('data', data);

      let user = state;
      user.name = data.fullname;
      user.business_name = data.business_name ? data.business_name : user.business_name;
      user.email = data.email;
      user.birthdate = data.birthdate;
      console.log('user', user);
      console.log('action.payload', action.payload);
      
      // if (user.address && user.address.length) {
      //   let address = user.address[0];
      //   address.street = data.street;
      //   address.country_id = data.country_id;
      //   address.region_id = data.region_id;
      //   address.city_id = data.city_id;
      // }

      return action.payload && action.payload.success
        ? {...user}
        : state;
    }
    case Auth.UPDATE_PROFILE_FAILED: {
      return state;
    }

    //CHANGE PASSWORD
    case Auth.CHANGE_PASSWORD: {
      return state;
    }
    case Auth.CHANGE_PASSWORD_SUCCESS: {
      return state;
    }
    case Auth.CHANGE_PASSWORD_FAILED: {
      return state;
    }

    //TOGGLE SERVICE STATUS
    case Auth.TOGGLE_SERVICE_STATUS: {
      return state;
    }
    case Auth.TOGGLE_SERVICE_STATUS_SUCCESS: {
      if (action.payload && action.payload.success) {
        const partner_services = state.partner_services;
        const targetIndex = partner_services.findIndex(service => {
          return service.id === action.payload.data.partner_service_id;
        });
        const target = partner_services[targetIndex];
        target.status = target.status !== 'active' ? 'active' : 'disable';
        state.partner_services[targetIndex] = target;
      }
      return state;
    }
    case Auth.TOGGLE_SERVICE_STATUS_FAILED: {
      return state;
    }

    //TOGGLE NOTIFICATION STATUS
    case Auth.TOGGLE_NOTIFICATION_STATUS: {
      if (action.payload.data && action.payload.data.type) {
        state[action.payload.data.type] = action.payload.data.value;
      }
      return state;
    }
    case Auth.TOGGLE_NOTIFICATION_STATUS_SUCCESS: {
      if (action.payload && action.payload.success) {
        state[action.payload.data.type] = action.payload.data.value;
      }
      return state;
    }
    case Auth.TOGGLE_NOTIFICATION_STATUS_FAILED: {
      if (action.payload.data && action.payload.data.type) {
        state[action.payload.data.type] = action.payload.data.value ? 0 : 1;
      }
      return state;
    }

    //PUSH_NOTIFICATION
    case Auth.PUSH_NOTIF_SWITCH: {
      const user = state;
      if (action.payload && action.payload.success && action.payload.mode) {
        user[action.payload.mode] = !user[action.payload.mode];
      }
      return user;
    }

    //UPDATE_PROFILE_IMAGE
    case Auth.UPDATE_PROFILE_IMAGE_SUCCESS:
      return action.payload.success
        ? {...state, ...{profile_image: action.payload.data}}
        : state;

    default:
      return state;
  }
}
