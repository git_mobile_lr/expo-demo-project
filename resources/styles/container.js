const container = {
    position: {
        relative: { position: "relative" },
        absolute: { position: "absolute" }
    }
}

export { container }