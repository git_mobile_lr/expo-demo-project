import colors from '@styles/palette.styles'

const button = {
    style: {
        paddingHorizontal: 30,
        paddingVertical: 11,
        borderRadius: 100
    },
    yellow: { backgroundColor: colors.yellow }
}

export { button }