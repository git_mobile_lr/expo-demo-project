import colors from '@styles/palette.styles'

const sfProRegular = "SFProText-Regular";
const sfProLight = "SFProText-Light";
const sfProMedium = "SFProText-Medium";
const sfProSemibold = "SFProText-Semibold";
const sfProBold = "SFProText-Bold";

const font = {
    size: {
        xl: { fontSize: 26 },
        lg: { fontSize: 20 },
        mlg: { fontSize: 18 },
        md: { fontSize: 15 },
        sm: { fontSize: 13 },
        xs: { fontSize: 12 }
    },
    weight: {
        regular: { fontFamily: sfProRegular },
        medium: { fontFamily: sfProMedium },
        bold: { fontFamily: sfProBold },
        semiBold: { fontFamily: sfProSemibold },
        light: { fontFamily: sfProLight }
    },
    color: {
        white: { color: '#ffffff' },
        black: { color: '#000000' },
        blue: { color: colors.blue },
        red: { color: colors.red },
        yellow: { color: colors.yellow },
        orange: { color: colors.orange },
        lightYellow: { color: colors.lightYellow },
        lightGray: { color: colors.lightGray },
        gray: { color: colors.gray },
        darkGray: { color: colors.darkGray }
    },
    align: {
        center: { textAlign: "center" },
        right: { textAlign: "right" },
        left: { textAlign: "left" }
    }
};
  
export { font };
  