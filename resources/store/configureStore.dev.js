import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import {createLogger} from 'redux-logger';
import ReduxPromise from 'redux-promise-middleware';
import {persistStore, persistReducer} from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import rootReducer from '../reducers';
import initialState from '../reducers/initialState';

const logger = createLogger({collapsed: true});
const persistConfig = {
  key: 'root',
  whitelist: [
    'user',
    'cart',
    'dashboard',
    'fbData',
    'categories',
    'countries',
    'location',
    'jobRequest',
    'jobOngoing',
  ],
  storage: AsyncStorage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);
const composeEnhancers = compose;

const store = createStore(
  persistedReducer,
  initialState,
  composeEnhancers(applyMiddleware(thunk, ReduxPromise, logger)),
);

const persistor = persistStore(store);

export {store, persistor};
