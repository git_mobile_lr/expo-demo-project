import React, { useState, useEffect } from 'react';
import { View } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import * as Layout from 'components/Layout';
import { CheckoutSubmitButton, MapPicker, MessagePopup } from 'components/common';
import {
    CheckoutHeader,
    CheckoutDeliveryInfo,
    CheckoutItemGroup,
    CheckoutBilling,
    CheckoutPaymentMethod,
    PaymentOptions
} from './components';

import { getSelectedTotalAmount } from 'helpers'
import CheckoutContext from 'contexts/checkout.context'
const MyCart = ({ route }) => {
    const [ showPayment, setShowPayment ] = useState(false);
    const navigation = useNavigation();
    const [ mounted, setMounted ] = useState(false)
    const [ merchant, setMerchant ] = useState(null)
    const [ items, setItems ] = useState(null)
    const [ payment, setPayment ] = useState("COD")
    const [ pickLocation, setPickLocation ] = useState(false)
    const togglePayment = status => setShowPayment(status)
    const handlePaymentSelect = selectedPayment => {
        if (selectedPayment == 'COD') {
            setPayment(selectedPayment);
        } else {
            MessagePopup.show({
                title: 'Coming soon!',
                message: `${selectedPayment} will be available soon.`,
                actions: [
                    {
                        text: 'Okay',
                        action: () => {
                                MessagePopup.hide();
                        },
                    },
                ],
            });
            setShowPayment(false);
        }
    }
    useEffect(() => {
        const { merchantCart, selectedItems } = route.params
        if (merchantCart && selectedItems) {
            setMerchant(merchantCart)
            setItems(selectedItems)
            setMounted(true)
        }
        else navigation.goBack()
    }, [])

    useEffect(() => {
        setShowPayment(false)
    }, [payment])

    const handleEditLocation = () => {
        console.log('handleEditLocation');
        setPickLocation(true);
    };

    const handleOnMapPick = () => {
        console.log('handleEditLocation');
        setPickLocation(false);
    };

    return mounted ? (
        <>
            <MapPicker active={pickLocation} onMapPick={handleOnMapPick} />
            <Layout.Wrapper style={{ backgroundColor: "#F8F8F8" }}>
                <CheckoutHeader />
                <CheckoutContext.Provider
                    value={{
                        payment,
                        merchant,
                        items,
                        showPayment,
                        togglePayment,
                        onPaymentSelect: handlePaymentSelect
                    }}
                >
                    <Layout.Scroll>
                        <CheckoutDeliveryInfo handleEditLocation={handleEditLocation}/>
                        <View style={{ padding: 15 }}>
                            <CheckoutItemGroup />
                            <View  style={ styles.dashedBorder } />
                        </View>
                        <CheckoutBilling />
                        <CheckoutPaymentMethod payment={ payment } toggle={ status => setShowPayment(status)} />
                    </Layout.Scroll>
                    <CheckoutSubmitButton total={ getSelectedTotalAmount(items) } onSubmit={ () => navigation.navigate("OneMoreStep") } />
                    <PaymentOptions />
                </CheckoutContext.Provider>
            </Layout.Wrapper>
        </>
    ) : null;
};

const styles = {
    dashedBorder: {
        height: 0,
        width: "100%",
        borderWidth: 2,
        borderStyle: 'dashed',
        borderColor: "#888",
        opacity: 0.5,
        borderRadius: 2,
        marginTop: 25,
        paddingTop: 1,
    }
}

export default MyCart;