import React, { useState } from 'react';
import { View } from 'react-native';
import { Input } from 'react-native-elements';
import { common } from 'styles/';

const CheckoutBilling = () => {
    const [ code, setCode ] = useState('');

    return (
        <View style={[ styles.wrapper ]}>
            {/* <View style={[ flex.direction.row, flex.justify.between ]}>
                <Text style={[ text.size.sm, text.weight.regular ]}>
                    Subtotal:
                </Text>
                <Text style={[ text.size.sm, text.weight.regular ]}>
                    P123.00
                </Text>
            </View>
            <View style={[ flex.direction.row, flex.justify.between, { marginTop: 10 } ]}>
                <Text style={[ text.size.sm, text.weight.regular ]}>
                    Total Delivery Fee:
                </Text>
                <Text style={[ text.size.sm, text.weight.regular ]}>
                    P123.00
                </Text>
            </View> */}
            <Input
                placeholder='Enter voucher code'
                { ...common.checkoutCouponInput }
                value={ code }
                onChangeText={ text => setCode(text) }
            />
            {/* <View style={[ flex.direction.row, flex.justify.between, { marginTop: 30 } ]}>
                <Text style={[ text.size.md, text.weight.medium, flex.align.center ]}>
                    Total Payment
                </Text>
                <Price style={[ text.size.mlg, text.weight.medium ]} value={ getSelectedTotalAmount(items) } />
            </View> */}
        </View>
    );
};

const styles = {
    wrapper: {
        padding: 20,
        backgroundColor: "#fff",
        marginTop: 10
    }
}

export { CheckoutBilling };