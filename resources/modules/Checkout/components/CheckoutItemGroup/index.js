import React, { useContext } from 'react';
import { View, Text } from 'react-native';

import {
    CheckoutGroupTag,
    CheckoutGroupMerch,
    CheckoutItem,
    CheckoutDeliveryFee
} from './components';
import { Price } from 'components/common'
import { textStyles as text, flex } from 'styles/';

import { getSelectedTotalAmount } from 'helpers'
import CheckoutContext from 'contexts/checkout.context'
const CheckoutItemGroupMemo = () => {
    const { items } = useContext(CheckoutContext)
    return (
        <View style={[ styles.wrapper ]}>
            <CheckoutGroupTag />
            <View style={ styles.details }>
                <CheckoutGroupMerch />
                { items.map((item, i) => <CheckoutItem key={`checkoutitem${ i }`} item={ item } />) }
                <CheckoutDeliveryFee />
                <View style={[ flex.direction.row, flex.justify.between, { marginTop: 10 } ]}>
                    <Text style={[ text.size.sm, text.weight.regular ]}>
                        Subtotal:
                    </Text>
                    <Price style={[ text.size.sm, text.weight.bold ]} value={ getSelectedTotalAmount(items) } />
                </View>
            </View>
        </View>
    );
};

const styles = {
    wrapper: {
        marginTop: 10,
        shadowColor: "#000000aa",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
    },
    details: {
        backgroundColor: "#fff",
        padding: 15
    }
};

const CheckoutItemGroup = React.memo(CheckoutItemGroupMemo);
export { CheckoutItemGroup };