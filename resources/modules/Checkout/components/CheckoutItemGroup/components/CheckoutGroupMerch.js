import React from 'react';
import { View, Text, Image } from 'react-native';

import { textStyles as text, flex } from 'styles/';

const CheckoutGroupMerch = () => {

    return (
        <View style={[ flex.direction.row, flex.align.center ]}>
            <Image
                resizeMode="cover"
                source={ require("/img/icon.png") }
                style={ styles.image }
            />
            <Text style={[ text.weight.regular, text.size.sm, { paddingHorizontal: 10, flex: 1 } ]}>
                Kape Natividad
            </Text>
            <Text style={[ text.weight.regular, text.size.sm, text.color.yellow ]}>
                Delivery Now
            </Text>
        </View>
    );
};

const styles = {
    image: {
        height: 20,
        width: 20,
        borderRadius: 50
    },
}
export { CheckoutGroupMerch };