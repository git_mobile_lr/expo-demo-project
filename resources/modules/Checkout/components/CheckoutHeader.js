import React from 'react';
import {
    View,
    Text
} from 'react-native';

import { BackButton } from 'components/common';

import { flex, textStyles as text } from 'styles/';

const CheckoutHeader = () => {

    return (
        <View style={ styles.wrapper }>
            <View style={ flex.justify.center } >
                <Text style={[ text.weight.regular, text.size.mlg, text.align.center ]}>
                    Checkout
                </Text>
                <BackButton containerStyle={{ position: "absolute", backgroundColor: "#B8B8B814" }} />
            </View>
        </View>
    );
};

const styles = {
    wrapper: {
        backgroundColor: "white",
        padding: 20,
        paddingTop: 50
    }
}

export { CheckoutHeader };