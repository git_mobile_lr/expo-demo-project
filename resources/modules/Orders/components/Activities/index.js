import React from 'react';
import {
    View,
    Text,
    TouchableOpacity,
} from 'react-native';

import { ActivityCard } from './components';

import palette from 'styles/palette.styles';
import textStyles from 'styles/textStyles.styles';
import flex from 'styles/flex.styles';

const Activities = () => {

    return (
        <View style={[ { marginTop: 30 } ]}>
            <View style={[ flex.direction.row, flex.align.center ]}>
                <Text style={[ textStyles.size.md, textStyles.weight.md ]}>
                    For Pick-up
                </Text>
                <Text style={[ textStyles.size.md, textStyles.weight.md, styles.count ]}>
                    1
                </Text>
            </View>
            {
                [...Array(5)].map((v, index) => <ActivityCard key={`activity${ index }`}/>)
            }
        </View>
    );
};

const styles = {
    count: {
        paddingVertical: 4,
        paddingHorizontal: 9,
        backgroundColor: palette.yellow,
        borderRadius: 20,
        marginLeft: 10
    }
}

export { Activities };