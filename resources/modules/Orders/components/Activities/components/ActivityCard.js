import React from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Image
} from 'react-native';
import { useNavigation } from '@react-navigation/native';

import palette from 'styles/palette.styles';
import textStyles from 'styles/textStyles.styles';
import flex from 'styles/flex.styles';

const ActivityCard = () => {
    const acitivityRandomness = [...Array(Math.floor(Math.random() * 4) + 1)];
    const navigation = useNavigation();

    return (
        <TouchableOpacity
            style={[ styles.container, { flex: 1 } ]}
            onPress={ () => navigation.navigate("OrderDetails") }
        >
            <View style={[ flex.direction.row ]}>
                <View style={[ flex.direction.row, flex.align.center, { flex: 1 } ]}>
                    <Image
                        resizeMode="cover"
                        source={require("img/icon.png")}
                        style={[ styles.merchImage ]}
                    />
                    <View style={{ marginLeft: 10 }}>
                        <Text style={[ textStyles.size.md, textStyles.weight.regular ]}>
                            Kape Natividad
                        </Text>
                        <Text style={[ textStyles.size.sm, textStyles.weight.regular, textStyles.color.darkGray ]}>
                            Delivery now
                        </Text>
                    </View>
                </View>
                <Text style={[ textStyles.size.md, textStyles.weight.regular ]}>
                    P845.00
                </Text>
            </View>
            <View style={{ marginTop: 25 }}>
                {
                    acitivityRandomness.map((v, index) => (
                        <View style={[ styles.activityLog, flex.direction.row ]} key={`activitylog${ index }`}>
                            <View style={[ flex.align.center, { width: 30, height: "100%" }]}>
                                {  acitivityRandomness.length !== (index + 1) ? <View style={[ styles.logLine ]}/> : null  }
                                <View style={[ styles.activityDot, index === 0 ? { backgroundColor: palette.yellow } : {} ]} />
                            </View>
                            <View style={{ marginLeft: 10, paddingBottom: 10 }}>
                                <Text style={[ textStyles.size.sm, textStyles.weight.regular, { lineHeight: 13, marginBottom: 7 } ]}>
                                    Kape Natividad
                                </Text>
                                <Text style={[ textStyles.size.sm, textStyles.weight.regular, textStyles.color.darkGray ]}>
                                    21 St. Natividad, Diliman VIllage, Diliman…
                                </Text>
                            </View>
                        </View>
                    ))
                }
            </View>
        </TouchableOpacity>
    );
};

const styles = {
    container: {
        paddingVertical: 25,
        paddingHorizontal: 15,
        backgroundColor: "#fff",
        borderRadius: 10,
        marginTop: 15,
    },
    merchImage: {
        width: 30,
        height: 30,
        borderRadius: 100
    },
    activityDot: {
        height: 8,
        width: 8,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: palette.yellow,
        backgroundColor: "#fff"
    },
    logLine: {
        position: "absolute",
        height: "100%",
        width: 0,
        borderWidth:1,
        borderStyle: 'dashed',
        borderColor: "#ccc",
        borderRadius: 1
    }
}

export { ActivityCard };