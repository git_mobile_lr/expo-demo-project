import React, { useState } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Image
} from 'react-native';

import palette from 'styles/palette.styles';
import textStyles from 'styles/textStyles.styles';
import flex from 'styles/flex.styles';

const StatusFilter = () => {
    const [ activeStatus, setActiveStatus ] = useState(0);

    return (
        <View style={[ flex.direction.row, flex.justify.between, { marginTop: 10 } ]}>
            {
                dummyStatuses.map(( status, index ) => (
                    <TouchableOpacity
                        key={ `status${ index }` }
                        onPress={ () => setActiveStatus(index) }
                    >
                        <View style={[ styles.imageContainer, flex.alignSelf.center, { backgroundColor: activeStatus === index ? palette.yellow : "#fff"} ]}>
                            <Image
                                resizeMode="contain"
                                source={ status.icon }
                                style={[ styles.image, flex.alignSelf.center ]}
                            />
                            { Math.floor(Math.random() * 2) === 0 ? <View style={[ styles.notification ]}/> : null }
                        </View>
                        <Text style={[ flex.alignSelf.center, textStyles.size.sm, textStyles.weight.medium, textStyles.color.darkGray ]}>
                            { status.name }
                        </Text>
                    </TouchableOpacity>
                ))
            }
        </View>
    );
};

const styles = {
    imageContainer: {
        padding: 12,
        borderRadius: 100,
        backgroundColor: "#fff",
        marginBottom: 7
    },
    image: {
        width: 30,
        height: 30,
    },
    notification: {
        position: "absolute",
        width: 12,
        height: 12,
        backgroundColor: palette.yellow,
        top: 0,
        right: 0,
        borderRadius: 12
    }
}

const dummyStatuses = [
    {
        name: "Scheduled",
        icon: require("img/delivery.png")
    },
    {
        name: "For Pick-up",
        icon: require("img/pickup.png")
    },
    {
        name: "To Recipient",
        icon: require("img/recipient.png")
    },
    {
        name: "To Rate",
        icon: require("img/thumbs-up.png")
    }
]

export { StatusFilter };