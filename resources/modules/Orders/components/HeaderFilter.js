import React, { useState } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
} from 'react-native';

import palette from 'styles/palette.styles';
import textStyles from 'styles/textStyles.styles';
import flex from 'styles/flex.styles';

const HeaderFilter = () => {
    const [ activeStatus, setActiveStatus ] = useState(0);

    return (
        <View style={[ flex.direction.row ]}>
            <TouchableOpacity
                onPress={ () => setActiveStatus(0) }
                style={{ flex: 1, padding: 10 }}
            >
                <Text style={[ flex.alignSelf.center, textStyles.size.md, textStyles.weight.medium, { color: activeStatus === 0 ? palette.yellow : palette.darkGray } ]}>
                    Ongoing
                </Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={ () => setActiveStatus(1) }
                style={{ flex: 1, padding: 10 }}
            >
                <Text style={[ flex.alignSelf.center, textStyles.size.md, textStyles.weight.medium, { color: activeStatus === 1 ? palette.yellow : palette.darkGray } ]}>
                    Past
                </Text>
            </TouchableOpacity>
        </View>
    );
};

export { HeaderFilter };