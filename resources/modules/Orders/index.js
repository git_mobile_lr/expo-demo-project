import React, { Component } from 'react';
import {
    ScrollView,
    View,
    Image,
    Text,
    TouchableOpacity,
    ImageBackground,
} from 'react-native';

import {
    HeaderFilter,
    StatusFilter,
    Activities
} from './components';

import palette from 'styles/palette.styles';
import textStyles from 'styles/textStyles.styles';
import flex from 'styles/flex.styles';

const Orders = () => {
    return (
        <View style={{ flex: 1, backgroundColor: palette.searchBar }}>
            <View style={{ backgroundColor: "#fff" }}>
                <Text style={[ textStyles.size.mlg, textStyles.weight.regular, textStyles.color.darkGray, flex.alignSelf.center, { padding: 20 } ]}>
                    My Activity
                </Text>
                <HeaderFilter />
            </View>
            <ScrollView
                showsVerticalScrollIndicator={false}
                contentInsetAdjustmentBehavior="automatic"
            >
                <View style={{ padding: 20 }}>
                    <StatusFilter />
                    <Activities />
                </View>
            </ScrollView>
        </View>
    );
};


export default Orders;