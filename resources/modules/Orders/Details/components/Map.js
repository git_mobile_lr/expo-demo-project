import React from 'react';
import {
    View
} from 'react-native';
import { BackButton } from 'components/common';

const Map = () => {

    return (
        <View style={ styles.mapWrapper }>
            <BackButton containerStyle={{ backgroundColor: "#000", marginLeft: 20, marginTop: 20 }} iconStyle={{ color: "#fff" }}/>
        </View>
    );
};

const styles = {
    mapWrapper: {
        height: 365,
        overflow: "hidden",
        backgroundColor: "#ccc",
        paddingTop: 20
    }
}

export { Map }