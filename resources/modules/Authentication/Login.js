/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState } from 'react';
import {
    ScrollView,
    View,
    Image,
    StyleSheet,
    StatusBar,
    Text,
    TouchableOpacity,
    Alert
} from 'react-native';

import { Input } from 'react-native-elements';
import { BackButton } from 'components/common';
import { useDispatch } from 'react-redux';

import { login, getUserData } from 'actions/auth.actions';
import {Auth, Cart} from '../../constants';

import palette from 'styles/palette.styles';
import textStyles from 'styles/textStyles.styles';
import formInputs from 'styles/formInputs.styles';
import * as InputValidator from '../../helpers/InputValidator';

const Login = ({ navigation }) => {
    const [user, setUser] = useState(null);
    const [email, setEmail] = useState(null);
    const [password, setPassword] = useState(null);
    const [validator, setValidator] = useState(null);
    const dispatch = useDispatch();

    const submitForm = () => {
        const isInputValid = validatePost();
        if (isInputValid) {
            let  params = {
                email: email,
                password: password
            };
            dispatch({
                type: Auth.LOGIN,
            })
            login(params)
            .then(res => {
                if (res.payload.data) {
                    dispatch({
                        type: Auth.LOGIN_SUCCESS,
                        payload: res.payload.data
                    });

                    dispatch({
                        type: Cart.GET_CART_SUCCESS,
                        payload: res.payload.data.cart
                    });
                    navigation.replace("Dashboard");
                } else {
                    Alert.alert(
                        "Notification",
                        res.payload.message
                    );
                }
            })
            .catch(error => {
                console.log(error);
                dispatch({
                    type: Auth.LOGIN_FAILED,
                })
                Alert.alert(
                    "Notification",
                    error.data.message
                );
            });
        };
    };

    const validatePost = () => {
        const input = {
            email,
            password
        };

        const required = {
            email: {name: 'E-mail', email: true},
            password: {name: 'Password', min: 6}
        };

        const validator = InputValidator.check(input, required);
        setValidator(validator);
        return validator.isInputValid;
    };

    return (
        <View style={styles.landing}>
            <ScrollView
                showsVerticalScrollIndicator={false}
                contentInsetAdjustmentBehavior="automatic" style={{flex: 1}}>
                <View style={styles.header}>
                    <BackButton
                        onPress={() => navigation.navigate("Landing")}
                        containerStyle={styles.backBtnContainerStyle}
                    />
                    <View 
                        style={styles.headerTextContainer}
                    >
                        <Text style={styles.headerText}>
                            Log in
                        </Text>
                    </View>
                </View>
                <View style={{marginHorizontal: -10}}>
                    <Input
                        inputStyle={formInputs.input}
                        inputContainerStyle={validator && validator.errors && validator.errors.email ? {...formInputs.inputContainer, ...formInputs.inputContainerError} : formInputs.inputContainer}
                        labelStyle={[formInputs.label, textStyles.normalTextRegular]}
                        label="Email Address"
                        placeholder="email@address.com"
                        onChangeText={(email) => setEmail(email)}
                        keyboardType={"email-address"}
                    />
                    <Input placeholder="Password" 
                        inputContainerStyle={validator && validator.errors && validator.errors.password ? {...formInputs.inputContainer, ...formInputs.inputContainerError} : formInputs.inputContainer}
                        inputStyle={formInputs.input}
                        labelStyle={[formInputs.label, textStyles.normalTextRegular]}
                        label="Password"
                        secureTextEntry={true} 
                        onChangeText={(password) => setPassword(password)}
                    />
                </View>
                <View style={{justifyContent: 'flex-end'}}>  
                    <TouchableOpacity style={styles.login} onPress={ submitForm }>
                        <Text style={[textStyles.mdTextBold, {color: palette.white}]}>Log In</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    header: {
        paddingVertical: 10,
        marginBottom: 50,
        flex: 1,
        flexDirection: "row",
        flexWrap: "wrap",
        alignItems: "center",
    },
    logo: {
        maxWidth: 220,
        height: 70,
        alignSelf: 'center'
    },
    landing: {
        flex: 1,
        backgroundColor: palette.white, 
        paddingHorizontal: 20,
        justifyContent: 'center',
    },
    login: {
        backgroundColor: palette.yellow,
        alignItems: 'center',
        marginBottom: 10,
        paddingVertical: 15,
        borderRadius: 5
    },
    headerText: {
        fontSize: 18,
        color: palette.secondaryBlack,
        fontWeight: 'bold',
    },
    headerTextContainer: {
        alignItems: 'center',
        paddingRight: 30,
        flex: 1,
    },
    backBtnContainerStyle: {
        flexBasis: 'auto',
    } 
});

export default Login;