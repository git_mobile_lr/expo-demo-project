/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
    ScrollView,
    View,
    Image,
    StyleSheet,
    StatusBar,
    Text,
    TouchableOpacity,
} from 'react-native';

import { useNavigation } from '@react-navigation/native';

import palette from 'styles/palette.styles';
import textStyles from 'styles/textStyles.styles';

const Landing = () => {
    const navigation = useNavigation();
    return (
        <View style={{ backgroundColor: palette.yellow, flex: 1 }}>
            <StatusBar barStyle="dark-content" backgroundColor={palette.yellow} />
            <ScrollView
                showsVerticalScrollIndicator={false}
                contentInsetAdjustmentBehavior="automatic" style={{flex: 1}}>
                <View style={styles.landing}>
                    <Image
                        resizeMode="stretch"
                        source={require('../../img/logo.png')}
                        style={styles.logo}
                    />
                </View>
                <View style={styles.buttonContainer}>  
                    <TouchableOpacity style={styles.login} onPress={ () => navigation.navigate("Login") }>
                        <Text style={[styles.loginText, textStyles.mdTextBold]}>Log In</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.signup} onPress={ () => navigation.navigate("Signup") }>
                        <Text style={[styles.signupText, textStyles.mdTextBold]}>New to Cyclehouse? Sign up!</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
	header: {
		paddingVertical: 7,
		paddingHorizontal: 10
	},
	logo: {
		maxWidth: 255,
		height: 80,
        marginTop: 50,
        alignSelf: 'center'
	},
    landing: {
        flex: 1,
        backgroundColor: palette.yellow,
        marginBottom: 50
    },
    buttonContainer: {
        justifyContent: 'flex-end'
    },
    login: {
        backgroundColor: palette.red,
        alignItems: 'center',
        marginHorizontal: 20,
        marginBottom: 10,
        paddingVertical: 15,
        borderRadius: 5
    },
    signup: {
        backgroundColor: palette.searchBar,
        alignItems: 'center',
        marginHorizontal: 20,
        paddingVertical: 15,
        borderRadius: 5
    },
    loginText: {
        fontSize: 18,
        color: palette.white,
    },
    signupText: {
        fontSize: 18,
        color: palette.secondaryBlack,
    }
});

export default Landing;