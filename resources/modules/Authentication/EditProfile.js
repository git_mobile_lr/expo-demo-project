/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState } from 'react';
import {
    ScrollView,
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
    Alert,
} from 'react-native';
import { Input } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';
import { useDispatch } from 'react-redux';

import { BackButton } from 'components/common';

import { signup, logout } from 'actions/auth.actions';
import { updateProfile } from 'actions/user.actions';
import {Auth} from '../../constants';

import palette from 'styles/palette.styles';
import textStyles from 'styles/textStyles.styles';
import formInputs from 'styles/formInputs.styles';
import { LoadingOverlay } from '../../components/common/';
import * as InputValidator from '../../helpers/InputValidator';

const EditProfile = ({ user }) => {
    const navigation = useNavigation();
    const [fullname, setFullName] = useState(user.name);
    const [email, setEmail] = useState(user.email);
    const [phone_number, setPhoneNumber] = useState(user.phone_number);
    const [password, setPassword] = useState(null);
    const [validator, setValidator] = useState(null);
    const dispatch = useDispatch();

    const submitForm = () => {
        LoadingOverlay.show('Updating profile data.');
        const isInputValid = validatePost();
        if (isInputValid) {
            let  params = {
                fullname: fullname,
                phone_number: phone_number,
                email: email,
                password: password,
                type: 'regular'
            };
            updateProfile(params)(dispatch)
            .then((res) => {
                LoadingOverlay.hide();
            })
            .catch((err) => {
                LoadingOverlay.hide();
            });
        } else {
            LoadingOverlay.hide();
        }
    };

    const validatePost = () => {
        const input = {
            fullname,
            email,
            phone_number,
        };

        const required = {
            fullname: 'Name',
            email: {name: 'E-mail', email: true},
            phone_number: {name: 'Phone Number', min: 11, max: 11},
        };

        const validator = InputValidator.check(input, required);
        setValidator(validator);
        return validator.isInputValid;
    };

    const handleLogout = () => {
        dispatch({
            type: Auth.LOGOUT,
        })
        logout()
        .then(res => {
            if (res.success) {
                navigation.navigate("Login");
            }
        });
    };
    return (
            <ScrollView
                showsVerticalScrollIndicator={false}
                contentInsetAdjustmentBehavior="automatic" style={styles.landing}>
                <View style={{marginHorizontal: -10}}>
                    <Input
                        inputStyle={formInputs.input}
                        inputContainerStyle={validator && validator.errors && validator.errors.fullname ? {...formInputs.inputContainer, ...formInputs.inputContainerError} : formInputs.inputContainer}
                        labelStyle={[formInputs.label, textStyles.normalTextRegular]}
                        label="Full Name"
                        onChangeText={(fullname) => setFullName(fullname)}
                        value={fullname}
                    />
                    <Input
                        inputStyle={formInputs.input}
                        inputContainerStyle={validator && validator.errors && validator.errors.email ? {...formInputs.inputContainer, ...formInputs.inputContainerError} : formInputs.inputContainer}
                        labelStyle={[formInputs.label, textStyles.normalTextRegular]}
                        label="Email Address"
                        onChangeText={(email) => setEmail(email)}
                        type="email"
                        value={email}
                    />
                    <Input
                        inputStyle={formInputs.input}
                        inputContainerStyle={validator && validator.errors && validator.errors.phone_number ? {...formInputs.inputContainer, ...formInputs.inputContainerError} : formInputs.inputContainer}
                        labelStyle={[formInputs.label, textStyles.normalTextRegular]}
                        label="Contact No."
                        onChangeText={(phone_number) => setPhoneNumber(phone_number)}
                        value={phone_number}
                    />
                   
                </View>
                <View style={{marginBottom: 20}}>  
                    <TouchableOpacity style={styles.signup} onPress={ submitForm }>
                        <Text style={[textStyles.mdTextBold, {color: palette.white}]}>Save Changes</Text>
                    </TouchableOpacity>
                    {/* <TouchableOpacity style={{padding: 10, alignItems: 'center'}} onPress={ handleLogout }>
                        <Text style={[textStyles.mdTextBold, {color: palette.secondaryBlack}]}>Log out</Text>
                    </TouchableOpacity> */}
                </View>
            </ScrollView>
    );
};

const styles = StyleSheet.create({
    header: {
        paddingVertical: 10,
        marginBottom: 50
    },
   
    landing: {
        flex: 1,
        backgroundColor: palette.white, 
        paddingVertical: 30,
        paddingHorizontal: 20,
    },
    signup: {
        backgroundColor: palette.yellow,
        alignItems: 'center',
        paddingVertical: 15,
        borderRadius: 5
    },
    signupText: {
        fontSize: 18,
        color: palette.secondaryBlack,
    }
});

export default EditProfile;