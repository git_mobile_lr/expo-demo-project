import React, { useEffect, useState, Component } from 'react';
import { View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { connect, useDispatch, useSelector } from 'react-redux';

import * as Layout from 'components/Layout';
import { CheckoutSubmitButton } from 'components/common';
import { MyCartHeader, MyCartItemGroup } from './components';
import { emptyCart, getCart } from 'actions/cart.actions';
import {Cart as CardActionTypes} from '../../constants';

import { cloneObject, getSelectedTotalAmount } from 'helpers'
import Cart from 'factories/cart.factory'

import CartContext from 'contexts/cart.context'
const MyCart = ({ route }) => {
    const navigation = useNavigation();
    const cart = useSelector(state => new Cart(state.cart))
    const [ merchantCart, setMerchantCart ] = useState(null)
    const [ selectedItems, setSelectedItems ] = useState([])
    const toggleAll = () => setSelectedItems(prev => prev.length !== merchantCart.items.length ? cloneObject(merchantCart.items) : [] )
    const toggleSelected = (item) => setSelectedItems(prev => {
        const newItems = cloneObject(prev)
        const isSelected = newItems.find( selected => item.id === selected.id )
        if (isSelected) return newItems.filter( selected => item.id !== selected.id )
        else newItems.push(item)
        
        return newItems
    })
    useEffect(() => {
        const selectedMerchant = route.params && route.params.merchant ? route.params.merchant : Object.keys(cart.merchants)[0];
        if (selectedMerchant) {
            const newMerchant = JSON.parse(JSON.stringify(cart.merchants[selectedMerchant]))
            setMerchantCart(newMerchant)
            setSelectedItems(cloneObject(newMerchant.items))
        }
        else navigation.goBack()
    }, [])

    return merchantCart && merchantCart.items.length ? (
        <Layout.Wrapper>
            <MyCartHeader />
            <CartContext.Provider
                value={{
                    merchantCart,
                    selectedItems,
                    toggleSelected,
                    toggleAll
                }}
            >
                <Layout.Scroll>
                    <View style={{ marginBottom: 100 }}>
                        <MyCartItemGroup
                            cart={ merchantCart }
                            selectedItems={ selectedItems }
                        />
                    </View>
                </Layout.Scroll>
                <CheckoutSubmitButton
                    total={ getSelectedTotalAmount(selectedItems) }
                    onSubmit={ () => navigation.navigate("Checkout", { merchantCart, selectedItems }) }
                    buttonText="Proceed to Checkout"
                    disabled={ !selectedItems.length }
                />
            </CartContext.Provider>
        </Layout.Wrapper>
    ) : null;
};


function mapStateToProps(state) {
	return {
	  cart: state.cart,
	};
}
  
   
export default connect(
	mapStateToProps,
	null,
)(MyCart);