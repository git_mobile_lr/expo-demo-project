import React from 'react';
import { View } from 'react-native';

import { MyCartItemMerch, MyCartItem } from './components';

const MyCartItemGroup = ({ cart, selectedItems }) => {
    const random = Math.floor(Math.random() * 4) + 1;
    
    return (
        <View style={{ marginBottom: 30, marginTop: 5 }}>
            <MyCartItemMerch />
            {
                cart.items.map((item, i) => (
                    <MyCartItem
                        key={`mycartitem${ i }`}
                        item={ item }
                        selected={ selectedItems.find(sItem => sItem.id === item.id) ? true : false }
                    />
                ))
            }
        </View>
    );
};

export { MyCartItemGroup };