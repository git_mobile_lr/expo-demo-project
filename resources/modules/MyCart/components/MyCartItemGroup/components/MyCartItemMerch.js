import React, { useContext } from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/Octicons';

import { CheckBox } from 'components/form';
import { flex, textStyles as text } from 'styles/';

import CartContext from 'contexts/cart.context'
const MyCartItemMerch = () => {
    const { merchantCart, selectedItems, toggleAll } = useContext(CartContext)
    return (
        <View style={[ styles.wrapper, flex.direction.row, flex.align.center ]}>
            <CheckBox
                offset={ 20 }
                initialValue={ selectedItems.length === merchantCart.items.length }
                onChange={ toggleAll  }
            />
            <View style={{ flex: 1 }}>
                <Text style={[ text.size.md, text.weight.regular ]}>
                    { merchantCart.name }
                </Text>
                <Text style={[ text.size.sm, text.weight.regular, text.color.darkGray ]}>
                    Delivery Now
                </Text>
            </View>
            <TouchableOpacity
                onPress={() => {}}
                style={{ padding: 20 }}
            >
                <Icon name="pencil" style={[ text.size.mlg, { color: "#B8B8B8" } ]}/>
            </TouchableOpacity>
        </View>
    );
};

const styles = {
    wrapper: {
        backgroundColor: "#fff",
        marginBottom: 12
    }
}
export { MyCartItemMerch };