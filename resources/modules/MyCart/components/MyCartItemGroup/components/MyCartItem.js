import React, { useContext } from 'react';
import {
    View,
    Text
} from 'react-native';

import { CheckBox } from 'components/form';
import { Image } from 'components/elements';
import { Price } from 'components/common'

import { flex, textStyles as text } from 'styles/';
import { URL } from 'config'

import CartContext from 'contexts/cart.context'
const MyCartItem = ({ item, selected }) => {
    const { toggleSelected } = useContext(CartContext)
    const random = Math.floor(Math.random() * 4) + 1;
    return (
        <View style={[ styles.wrapper, flex.direction.row ]}>
            <CheckBox
                offset={ 20 }
                initialValue={ selected }
                onChange={ () => toggleSelected(item) }
            />
            <View style={[ flex.align.center, flex.direction.row ]}>
                <Image
                    imgProps={{
                        resizeMode: "cover",
                        source:item.image ? { uri: `${ URL.base }${ item.asset.path }`} : require('img/meal.png') ,
                        style: styles.image
                    }}
                    containerStyle={{ paddingVertical: 20 }}
                />
                <View style={{ padding: 20 }}>
                    <Text style={[ text.size.md, text.weight.regular ]}>
                        { item.title }
                    </Text>
                    <Text style={[ text.size.sm, text.weight.regular, text.color.darkGray ]}>
                        { item.quantity }
                    </Text>
                    <Text style={[ text.size.md, text.weight.regular, { marginTop: 5 } ]}>
                        
                    </Text>
                    <Price style={[ text.size.md, text.weight.regular, { marginTop: 5 } ]} value={ item.price } />
                </View>
            </View>
        </View>
    );
};

const styles = {
    wrapper: {
        backgroundColor: "#fff"
    },
    image: {
        width: 60,
        height: 60,
        borderRadius: 5
    }
}
export { MyCartItem };