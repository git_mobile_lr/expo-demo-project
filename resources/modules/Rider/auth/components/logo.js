import React from 'react'
import { View, Image } from 'react-native'

import { bg, flex } from '@styles'
import logo from '@img/logo.png'

const Logo = ({ paddingVertical = 50, imgWidth = 234 }) => {

    return (
        <View style={[ styles.logoWrapper ]} >
            <View style={[ bg.color.yellow, styles.imgWrapper, flex.align.center, { paddingVertical } ]}>
                <Image
                    source={ logo }
                    style={[ styles.img, { width: imgWidth } ]}
                />
            </View>
        </View>
    )
}

const styles = {
    logoWrapper: { paddingRight: 40 },
    imgWrapper: {
        paddingHorizontal: 45,
        borderBottomRightRadius: 50
    },
    img: { resizeMode: "contain" }
}

export { Logo }