import React from 'react'
import { View, Text } from 'react-native'
import { font, flex } from '@styles'

const Header = ({ title }) => {
    return (
        <View style={[ styles.header, flex.direction.row, flex.justify.between ]}>
            <Text style={[ font.weight.bold, font.size.mlg ]}>
                { title }
            </Text>
            <View style={[ flex.justify.between, flex.align.end, { flex: 1 } ]}>
                <View style={[ styles.headerBar, { maxWidth: 87 } ]} />
                <View style={[ styles.headerBar, { maxWidth: 93 } ]} />
                <View style={[ styles.headerBar, { maxWidth: 20 } ]} />
            </View>
        </View>
    )
}

const styles = {
    header: { paddingLeft: 25 },
    headerBar: {
        height: 3,
        backgroundColor: "#000",
        width: "100%",
        borderTopLeftRadius: 10,
        borderBottomLeftRadius: 10
    }
}

export { Header }