import React, { useState } from 'react'
import { View } from 'react-native'
import { useDispatch } from 'react-redux';

import { login } from '@actions/auth.actions';
import { Auth } from '@constants/';
import { Input, Button } from '@components/form'
import { LoadingOverlay, MessagePopup } from '@components/common/'
import * as InputValidator from '@helpers/InputValidator'

const Form = () => {
    const [ form, setForm ] = useState({})
    const [ loading, setLoading ] = useState(false)
    const [validator, setValidator] = useState(null)
    const dispatch = useDispatch()
    const handleChange = ({ name, value }) => {
        setForm(prev => {
            const newForm = { ...prev }
            newForm[name] = value

            return newForm
        })
    }
    
    const handleLogin = () => {
        setLoading(true)
        LoadingOverlay.show('Loading.')
        const isInputValid = validatePost()
        if (isInputValid) {
            let  params = {
                phone_number: form.mobile,
                password: form.password
            };
            dispatch({
                type: Auth.LOGIN,
            })
            login(params)
            .then(res => {
                setLoading(false)
                LoadingOverlay.hide()
                if (res.payload.data) {
                    // dispatch({
                    //     type: Auth.LOGIN_SUCCESS,
                    //     payload: res.payload.data
                    // });

                    // navigation.replace("Dashboard");
                    MessagePopup.show({
                        title: 'Cyclehouse rider app is under maintenance!',
                        message: `${res.payload.data.name} please try again later.`,
                        actions: [
                                {
                                        text: 'Okay',
                                        action: () => {
                                                MessagePopup.hide();
                                        },
                                },
                        ],
                    });
                } else {
                    Alert.alert(
                        "Notification",
                        message
                    );
                }
            })
            .catch(error => {
                setLoading(false)
                LoadingOverlay.hide()
                console.log(error);
                // dispatch({
                //     type: Auth.LOGIN_FAILED,
                // })
                MessagePopup.show({
                    title: 'Please check!',
                    message: `${error.data.message}`,
                    actions: [
                            {
                                    text: 'Okay',
                                    action: () => {
                                            MessagePopup.hide();
                                    },
                            },
                    ],
                });
            });
        } else {
            setLoading(false)
            LoadingOverlay.hide()
        }
    }

    const validatePost = () => {
        const input = form;

        const required = {
            mobile: {name: 'Mobile number', min: 11, max: 11},
            password: {name: 'Password', min: 6}
        };

        const validator = InputValidator.check(input, required);
        setValidator(validator);
        return validator.isInputValid;
    };

    return (
        <View>
            <View style={{ padding: 25 }}>
                <Input
                    name="mobile"
                    label="Mobile Number"
                    onChange={ handleChange }
                    value={ form.mobile }
                    containerStyle={{ marginBottom: 10 }}
                    icon={{ name: "phone" }}
                    hasError={validator && validator.errors && validator.errors.mobile}
                />
                <Input
                    name="password"
                    label="Password"
                    onChange={ handleChange }
                    value={ form.password }
                    icon={{ name: "lock", type: "MaterialCommunityIcons" }}
                    password
                    hasError={validator && validator.errors && validator.errors.password}
                />
                <Button
                    text="Login"
                    buttonStyle={{ marginTop: 40 }}
                    onPress={ handleLogin }
                    disabled={ loading }
                />
            </View>
        </View>
    )
}

export { Form }