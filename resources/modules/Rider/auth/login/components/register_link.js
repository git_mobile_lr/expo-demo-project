import React from 'react'

import { Link } from '@components/common'
import { View, Text } from 'react-native'

import { font, flex } from '@styles'

const RegisterLink = () => {

    return (
        <View style={[ flex.direction.row, flex.justify.center, { paddingBottom: 50 } ]}>
            <Text style={[ font.weight.regular, font.size.md, { marginRight: 5 } ]}>
                Doesn't have account?
            </Text>
            <Link to="Register">
                <Text style={[ font.weight.regular, font.size.md, font.color.orange ]}>
                    Register here
                </Text>
            </Link>
        </View>
    )
}

export { RegisterLink }