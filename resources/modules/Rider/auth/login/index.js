import React from 'react'
import { View } from 'react-native'

import AuthWrapper from '@components/wrappers/auth_wrapper'
import { Logo, Header } from '../components'
import { Form, RegisterLink } from './components'

import { flex } from '@styles'

const Login = () => {

    return (
        <AuthWrapper>
            <View style={[ { flex: 1 }, flex.justify.between ]}>
                <Logo />
                <View>
                    <Header title="Login to your Account" />
                    <Form />
                </View>
                <RegisterLink />
            </View>
        </AuthWrapper>
    )
}

export default Login