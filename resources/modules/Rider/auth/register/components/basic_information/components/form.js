import React from 'react'
import { View } from 'react-native'

import { Input } from '@components/form'

const Form = ({ form, onChange, validator }) => {

    return (
        <View style={{ marginBottom: 50 }}>
            {
                inputMap.map(field => (
                    <Input
                        key={ field.name }
                        name={ field.name }
                        label={ field.label }
                        onChange={ onChange }
                        value={ form[field.name] }
                        containerStyle={{ marginTop: 10 }}
                        password={ field.password }
                        hasError={validator && validator.errors && validator.errors[field.name]}
                    />
                ))
            }
        </View>
    )
}

const inputMap = [
    {
        name: "full_name",
        label: "Full Name"
    },
    {
        name: "phone_number",
        label: "Mobile Number"
    },
    {
        name: "password",
        label: "Password",
        password: true
    },
    {
        name: "location",
        label: "Which city are you located in?"
    },
    {
        name: "email",
        label: "Email Address"       
    }
]

export { Form }