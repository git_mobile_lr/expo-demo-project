import React from 'react'
import { View, Image, Text, TouchableOpacity } from 'react-native'
import { Icon } from '@components/common'
import { flex, font, container } from '@styles'
import avatar from '@img/avatar.png'

import { toggleCamera } from '@actions/camera.actions'

const Photo = ({ image, onChange }) => {
    const captureCallback = (uri) => onChange({name: "photo", value: uri })

    return (
        <View style={[ flex.direction.row, flex.align.center, { marginTop: 25, marginBottom: 20 } ]}>
            <TouchableOpacity onPress={ () => { toggleCamera({ inUse: true, onCapture: captureCallback }) } }>
                <View style={[ container.position.relative ]}>
                    <Image
                        style={ styles.avatar }
                        source={ image ? { uri: `data:image/png;base64,${image}` } : avatar }
                    />
                    <Icon name="plus-circle" size={ 30 } iconStyle={[ font.color.yellow, styles.plusIcon ]} />
                </View>
            </TouchableOpacity>
            <View style={{ marginLeft: 30, flex: 1 }}>
                <Text style={[ font.size.md, font.weight.bold ]}>
                    Upload Profile Picture
                </Text>
                <Text style={[ font.size.sm, font.weight.regular, { marginTop: 10 } ]}>
                    For security, please make sure your face is clearly visible. Avoid blurry or too dark photos.
                </Text>
            </View>
        </View>
    )
}

const styles = {
    avatar: {
        width: 75,
        height: 75,
        borderRadius: 100,
        resizeMode: "cover"
    },
    plusIcon: {
        position: "absolute",
        right: 0,
        bottom: 0,
        backgroundColor: "#fff",
        borderRadius: 100
    }
}

export { Photo }