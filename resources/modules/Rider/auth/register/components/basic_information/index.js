import React, { useState } from 'react'
import { View, Text } from 'react-native'
import { Button } from '@components/form'
import { Photo, Form } from './components'
import { font } from '@styles'
import { useDispatch } from 'react-redux';

import { signupRider } from '@actions/auth.actions';
import { Auth } from '@constants/';
import { LoadingOverlay } from '@components/common/'
import * as InputValidator from '@helpers/InputValidator'

const BasicInformation = () => {
    const [ form, setForm ] = useState({})
    const [validator, setValidator] = useState(null)
    const dispatch = useDispatch()
    const handleOnChange = ({ name, value }) => {
        setForm(prev => {
            const newForm = { ...prev }
            newForm[name] = value

            return newForm
        })
    }
    const handleOnNext = () => {
        LoadingOverlay.show('Loading.')
        const isInputValid = validatePost();
        console.log('form.photo', form.photo);
        if (isInputValid) {
            const  params = {
                fullname,
                phone_number,
                email,
                password,
                type: 'regular'
            };
            dispatch({
                type: Auth.SIGNUP,
            })
            signup(params)
            .then(res => {
                LoadingOverlay.hide()
                if (res.payload.data) {
                    dispatch({
                        type: Auth.SIGNUP_SUCCESS,
                        payload: res.payload.data
                    });
                
                    navigation.navigate("Login");
                } else {
                
                }
            })
            .catch(error => {
                console.log(error);
                LoadingOverlay.hide()
                dispatch({
                    type: Auth.SIGNUP_FAILED,
                })
            });
        } else {
            LoadingOverlay.hide()
        }
    }

    const validatePost = () => {
        const input = form;

        const required = {
            full_name: {name: 'Name'},
            phone_number: {name: 'Mobile number', min: 11, max: 11},
            password: {name: 'Password', min: 6},
            location: {name: 'Location', min: 6},
            email: {name: 'E-mail', email: true},
        };

        const validator = InputValidator.check(input, required);
        setValidator(validator);
        return validator.isInputValid;
    };

    return (
        <View style={{ flex: 1 }}>
            <View style={{ flex: 1 }}>
                <Text style={[ font.size.sm, font.weight.regular ]}>
                    Basic Information
                </Text>
                <Photo
                    image={ form.photo }
                    onChange={ handleOnChange }
                />
                <Form form={ form } validator={ validator } onChange={ handleOnChange }/>
            </View>
            <View style={{ marginTop: 25 }}>
                <Button
                    text="Next"
                    onPress={ handleOnNext }
                />
            </View>
        </View>
    )
}

export { BasicInformation }