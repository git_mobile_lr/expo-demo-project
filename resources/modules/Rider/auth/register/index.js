import React from 'react'
import { View } from 'react-native'

import AuthWrapper from '@components/wrappers/auth_wrapper'
import { Logo, Header } from '../components'
import { BasicInformation } from './components'

const Register = () => {

    return (
        <AuthWrapper>
            <View style={[ { flex: 1 } ]}>
                <Logo paddingVertical={ 20 } imgWidth={ 168 } />
                <View style={{ flex: 1, paddingVertical: 25 }}>
                    <Header title="Create Account" />
                    <View style={{ flex: 1, paddingHorizontal: 25, marginTop: 25 }}>
                        <BasicInformation />
                    </View>
                </View>
            </View>
        </AuthWrapper>
    )
}

export default Register