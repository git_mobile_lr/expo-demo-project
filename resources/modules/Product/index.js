import React, { useEffect, useState } from 'react';
import {
    ScrollView,
    View,
    StatusBar
} from 'react-native';
import { Button } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';

import palette from 'styles/palette.styles';
import textStyles from 'styles/textStyles.styles';
import { findProduct } from 'actions/merchant.actions';

import {
    ProductHeader,
    ProductImages,
    ProductInfo,
    ProductQtyPrice,
    ProductVariantSelection,
    SimilarProducts
} from './components';

import { emptyCart, getCart } from 'actions/cart.actions';
import {Cart} from '../../constants';

const Product = ({ route }) => {
    const navigation = useNavigation();
    const [ product, setProduct ] = useState(null);
    const [ cartItem, setCartItem ] = useState([]);

    useEffect(() => {
        if (route.params && route.params.slug) {
            findProduct(route.params.slug)
                .then(res => {
                    console.log(res.data);
                    if(res.data) {
                        setProduct(res.data.product)
                    }  else {
                        setProduct({})
                    }
                })
        }
    }, [])

    return product ? (
        <View style={{ flex: 1, backgroundColor: palette.lightGray }}>
            <StatusBar translucent backgroundColor='transparent' />
            <ProductHeader />
			<ScrollView
				showsVerticalScrollIndicator={false}
				contentInsetAdjustmentBehavior="automatic"
                style={{ paddingHorizontal: 20 }}
            >
				<ProductImages product={ product } />
                <ProductInfo product={ product } />
                {
                    product.variant_groups && product.variant_groups.length > 0 ? 
                        product.variant_groups.map((variant, index) => (
                            <ProductVariantSelection
                                key={`productvariant${ index }`}
                                variant={ variant }
                            />       
                        )) : dummyVariants.map((variant, index) => (
                            <ProductVariantSelection
                                key={`productvariant${ index }`}
                                variant={ variant }
                            />   
                        ))
                }
                
                {/* <SimilarProducts /> */}
			</ScrollView>
            <View style={[ styles.stickyButton ]}>
                <Button
                    onPress={ () => {} }
                    titleStyle={[textStyles.size.sm, textStyles.weight.regular, {color: palette.black}]}
                    title="Add to Cart"
                    buttonStyle={{backgroundColor: palette.yellow, borderRadius: 100, width: "100%", padding: 17}}
                />
            </View>
		</View>
    ) : null;
};

const styles = {
    stickyButton: {
        position: "absolute",
        bottom: 0,
        left: 0,
        paddingHorizontal: 20,
        paddingVertical: 10,
        width: "100%"
    }
}

const dummmyVariants = [
    {
        name: "Variation",
        isRequired: true,
        options: [
            {
                name: "Medium",
                addedPrice: 20
            },
            {
                name: "Large",
                addedPrice: 40
            }
        ]
    },
    {
        name: "Add-ons",
        isRequired: false,
        options: [
            {
                name: "Expresso Shot",
                addedPrice: 30
            },
            {
                name: "Whipped Cream",
                addedPrice: 20
            }
        ]
    },
]

export default Product;