import React from 'react';
import {
    View,
	Dimensions,
    Image,
    ScrollView
} from 'react-native';

const ProductImages = () => {

    return (
        <View style={{ marginBottom: 15 }}>
            <ScrollView
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            contentInsetAdjustmentBehavior="automatic">
                {
                    dummyImages.map((image, index) => (
                        <Image
                            key={`productimage${ index }`}
                            resizeMode="cover"
                            source={require('img/restau-4.jpg')}
                            style={[ styles.image, { marginLeft: index === 0 ? 0 : 20 } ]}
                        />
                    ))
                }
            </ScrollView>
        </View>
    );
};

const dummyImages = [...Array(5)];

const styles = {
    image: {
        height: 200,
        width: 320,
        borderRadius: 10
    }
};

export { ProductImages };