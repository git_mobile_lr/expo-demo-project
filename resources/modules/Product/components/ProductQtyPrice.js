import React, { useState } from 'react';
import {
    Text,
    View
} from 'react-native';

import { ProductQuantityControl } from 'components/common';

import flex from 'styles/flex.styles';
import textStyles from 'styles/textStyles.styles';

const ProductQtyPrice = ({product}) => {
    const [ quantity, setQuantity ] = useState(0);
    const price = product.price;

    return (
        <View style={[ flex.align.center, flex.direction.row, { marginTop: 15 } ]} >
            <ProductQuantityControl quantity={ quantity }  onChange={ (value) => setQuantity(value) }/>
            <View style={[ flex.direction.row, flex.justify.end, { flex: 1 }]}>
                <Text style={[ textStyles.size.lg, textStyles.weight.bold ]} >
                    { (quantity ? (price * quantity) : price).toFixed(2) }
                </Text>
            </View>            
        </View>
    );
};

export { ProductQtyPrice };