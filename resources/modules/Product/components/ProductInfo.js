import React from 'react';
import {
    Text,
    View,
    TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/EvilIcons';
import HTML from "react-native-render-html";

import flex from 'styles/flex.styles';
import textStyles from 'styles/textStyles.styles';


const ProductInfo = ({product}) => {

    return (
        <View>
            <View style={[ flex.direction.row, flex.align.center, flex.justify.between ]}>
                <Text style={[ textStyles.size.lg, textStyles.weight.bold ]}>
                    {product.title}
                </Text>
                <TouchableOpacity style={[ styles.button ]}>
                    <Icon name="share-google" size={ 22 }/>
                </TouchableOpacity>
            </View>
            {
                product.description ? (
                    <HTML
                        tagsStyles={{ p: textStyles.color.darkGray }}
                        style={[ textStyles.size.sm, textStyles.weight.regular, textStyles.color.darkGray, { marginTop: 15 } ]}
                        source={{ html: product.description }}
                    />
                ) : null
            }
        </View>
    );
};

const styles = {
    button: {
        backgroundColor: "#FDF0D6",
        padding: 5,
        borderRadius: 5
    }
};

export { ProductInfo };