import React from 'react';

import {
    View,
    Text
} from 'react-native';

import { ProductCard } from 'components/common';

import textStyles from 'styles/textStyles.styles';
import flex from 'styles/flex.styles';

const SimilarProducts = () => {

    return (
        <View style={{ marginTop: 50, marginBottom: 70 }}>
            <Text style={[ textStyles.size.md, textStyles.weight.medium, { marginBottom: 5 } ]}>
                Similar Products
            </Text>
            <View style={[ flex.direction.row, flex.wrap, flex.justify.between ]}>
                <View style={ styles.gridCell }>
                    {
                        dummyProducts.map((product, index) => (
                            <ProductCard
                                product={dummyProducts[Math.floor(Math.random() * dummyProducts.length)]}
                                key={`productcell1${ index }`}
                                orientation='v'
                            />
                        ))
                    }
                </View>
                <View style={ styles.gridCell }>
                    {
                        dummyProducts.map((product, index) => (
                            <ProductCard
                                product={dummyProducts[Math.floor(Math.random() * dummyProducts.length)]}
                                key={`productcell2${ index }`}
                                orientation='v'
                            />
                        ))
                    }
                </View>
            </View>
        </View>
    );
};

const dummyProducts = [
    {
        name: "Cafe Latte",
        description: "Coffee mixed with milk",
        price: "120.00"
    },
    {
        name: "Cappucino",
        description: "Coffee mixed with milk",
        price: "120.00"
    },
    {
        name: "Cafe Mocha",
        description: "Coffee mixed with milk",
        price: "110.00"
    },
    {
        name: "White Chocolate Latte",
        description: "Coffee mixed with milk",
        price: "110.00"
    }
]

const styles = {
    gridCell: {
        width: "48%"
    }
}
export { SimilarProducts };