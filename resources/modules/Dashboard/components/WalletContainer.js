import React from 'react';
import {
	View,
	Image,
	Text
} from 'react-native';

import textStyles from 'styles/textStyles.styles';
import flex from 'styles/flex.styles';
import { white } from 'styles/palette.styles';

const WalletContainer = ({ credits, wallet }) => {
    
    return (
        <View style={{
                ...styles.walletContainer,
                ...flex.justify.between,
                ...flex.align.center,
                ...flex.direction.row
            }}
        >
            <View style={{ ...flex.align.center, ...flex.direction.row }}>
                <View style={{ ...styles.wallet, ...flex.align.center }}>
                    <Image
                        resizeMode="contain"
                        source={require('img/wallet.png')}
                        style={styles.walletIcon}
                    />
                </View>
                <Text style={[ textStyles.size.md ]}>PHP { wallet }</Text>
            </View>
            <View style={ styles.divider } />
            <View style={{ ...flex.align.center, ...flex.direction.row }}>
                <View style={styles.wallet}>
                    <Image
                        resizeMode="contain"
                        source={require('img/points.png')}
                        style={styles.walletIcon}
                    />
                </View>
                <Text style={[ textStyles.size.md ]}>{ credits } points</Text>
            </View>
        </View>
    );
};

const styles = {
    divider: {
		width: 1,
		height: 29,
		backgroundColor: '#C6C6C8'
	},
    walletContainer: {
		paddingVertical: 30,
	},
	wallet: {
		marginRight: 11,
		width: 45,
		height: 45,
		paddingVertical: 12,
		backgroundColor: white,
		borderRadius: 45
	},
	walletIcon: {
		maxHeight: 20
	},
}

export { WalletContainer };