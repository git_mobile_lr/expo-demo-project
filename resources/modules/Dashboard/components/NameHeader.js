import React from 'react';
import { useNavigation } from '@react-navigation/native';
import {
	View,
	Image,
	Text,
	TouchableOpacity,
    Alert,
} from 'react-native';

import textStyles from 'styles/textStyles.styles';
import { MessagePopup } from '../../../components/common/';

const NameHeader = ({ user, navigation, handleLogout }) => {
	
    return (
        <View style={styles.nameHeader}>
            <Text style={[ textStyles.weight.bold, textStyles.size.lg ]}>Hello </Text>
            <Text style={textStyles.size.lg}>{ user.name }!</Text>
            <View style={styles.profile}>
                <TouchableOpacity
                    onPress={ () => 
                        MessagePopup.show({
                            title: 'Account action',
                            actions: [
                                {
                                    text: 'View cart',
                                    action: () => {
                                        navigation.navigate('MyCart');
                                        MessagePopup.hide();
                                    },
                                },
                                {
                                    text: 'Edit Profile',
                                    action: () => {
                                        navigation.navigate('Account');
                                        MessagePopup.hide();
                                    },
                                },
                                {
                                    text: 'Logout',
                                    action: () => {
                                        handleLogout();
                                        MessagePopup.hide();
                                    },
                                },
                                {
                                    text: 'Close',
                                    action: () => {
                                        MessagePopup.hide();
                                    },
                                },
                            ],
                        })
                    }
                >
                <Image
                    resizeMode="cover"
                    source={require('img/profile.jpg')}
                    style={styles.profileImage}
                />
                </TouchableOpacity>
            </View>
        </View>
    );
};

const styles = {
    nameHeader: {
		flexDirection: 'row',
		alignItems: 'center'
	},
    profile: {
		flex: 1
	},
	profileImage: {
		width: 40,
		height: 40,
		borderRadius: 50,
		alignSelf: "flex-end"
	},
}

export { NameHeader };
