import React from 'react';
import {
	View,
	Text,
    Dimensions
} from 'react-native';
import Carousel from 'react-native-snap-carousel';

import { DealCard } from 'components/common';
import textStyles from 'styles/textStyles.styles';
import flex from 'styles/flex.styles';

const windowWidth = Dimensions.get("window").width;
const Deals = () => {
    
    return (
        <View style={{ marginBottom: 27 }}>
            <Text style={[ textStyles.weight.medium, textStyles.size.xs, flex.alignSelf.end ]}>See All Deals</Text>
            <Carousel
                data={ dummyDeals }
                renderItem={ ({item}) => <DealCard deal={ item }/> }
                sliderWidth={windowWidth - 40}
                itemWidth={windowWidth - 40}
            />
        </View>
    );
};

const dummyDeals = [
    {
        title: "Get 5% off on Japanese dishes",
        route: "Merchant"
    },
    {
        title: "Get 50% off to our essential lugaw",
        route: "Merchant"
    }
]


export { Deals };