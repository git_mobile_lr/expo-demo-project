import React, { useEffect,  useState } from 'react';
import {
	View,
	Text,
    ScrollView,
    Image,
    TouchableOpacity
} from 'react-native';
import { useNavigation } from '@react-navigation/native';

import { getMerchants } from 'actions/merchant.actions'

import textStyles from 'styles/textStyles.styles';
import palette from 'styles/palette.styles';
import { URL } from 'config'

const Restaurants = () => {
    const [ merchants, setMerchants ] = useState([])

    useEffect(() => {
        getMerchants()
            .then(res => res.data ? setMerchants(res.data) : {} )
    }, [])

    return merchants.length ? (
        <View>
            <Text style={[textStyles.size.md, textStyles.weight.medium , { marginBottom: 15 }]}>Merchants</Text>
            <ScrollView
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            contentInsetAdjustmentBehavior="automatic">
                {
                    merchants.map((merchant, index) => <RestaurantCard key={ `foodrestaurant${ index }` } merchant={ merchant }/>)
                }
            </ScrollView>
        </View>
    ) : null;
};

const RestaurantCard = ({ merchant }) => {
    const navigation = useNavigation();

    return (
        <TouchableOpacity onPress={ () => navigation.navigate("Merchant", { id: merchant.id }) }>
            <View style={styles.restaurant}>
                <Image
                    resizeMode="cover"
                    source={ merchant.image ? { uri: `${ URL.base }${ merchant.asset.medium_thumbnail }`} : logo }
                    style={styles.restaurantImage}
                />
                <View style={styles.restaurantInfo}>
                    <Text style={textStyles.normalTextBold}>{ merchant.name }</Text>
                </View>
            </View>
        </TouchableOpacity>
    );
}

const styles = {
    restaurant: {
		borderRadius: 6,
		margin: 5,
		width: 160,
		marginBottom: 10,
		overflow: 'hidden',
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.1,
		shadowRadius: 2,
		elevation: 3,
		backgroundColor: palette.white,
        flex: 1
	},
	restaurantImage: {
		height: 120,
	},
	restaurantInfo: {
		paddingHorizontal: 12,
		paddingVertical: 12
	},
}

export { Restaurants };