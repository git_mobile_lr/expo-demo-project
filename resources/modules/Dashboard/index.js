/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import {
	ScrollView,
	View,
	StatusBar,
	Text,
	TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import * as authActions from '../../actions/auth.actions';

import DashboardHeader from 'components/DashboardHeader';
import {
	NameHeader,
	WalletContainer,
	Categories,
	Deals,
	Restaurants
} from './components';

import palette from 'styles/palette.styles';
import dashboard from 'styles/dashboard.styles';
import { MessagePopup, MapPicker } from '../../components/common/';

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pickLocation: false,
    };
  }

	handleComingSoon = name => {
		MessagePopup.show({
			title: 'Coming soon!',
			message: `${name} will be available soon.`,
			actions: [
					{
							text: 'Okay',
							action: () => {
									MessagePopup.hide();
							},
					},
			],
		});
	}

	handleLogout = () => {
		const res = this.props.actions.logout();
		return (
			res.payload.success && this.props.navigation.navigate('Login')
		);
	}

	handleOnMapPick = input => {
		this.setState({
			pickLocation: false,
		});
	}

	render() {
		const { user, navigation, location} = this.props;
		const { pickLocation} = this.state;
		console.log('location', location.picked);
		return (
			<>
				<MapPicker active={pickLocation} onMapPick={this.handleOnMapPick} />
				<View style={{ flex: 1 }}>
					<StatusBar barStyle="dark-content" backgroundColor={palette.yellow} />
					<DashboardHeader />
					<ScrollView
						showsVerticalScrollIndicator={false}
						contentInsetAdjustmentBehavior="automatic">
						<View backgroundColor={palette.white} style={dashboard.scrollView}>
							<View backgroundColor={palette.lightGray} style={dashboard.dashboardHeader}>
								{user && (
									<>
										<NameHeader user={user} handleLogout={() => this.handleLogout()} navigation={navigation} />
										<TouchableOpacity
											style={{flexDirection: 'row', alignItems: 'center'}}
											onPress={() => this.setState({pickLocation: true})}>
											<Icon
												name="map-marker"
												size={20}
												color={palette.yellow}
											/>
											<Text>
												{location.picked ? location.picked.formatted_address : `Pick delivery location. `}
											</Text>
											{!location.picked && (
												<Icon
													name="pencil"
													size={10}
													color={palette.white}
													style={{backgroundColor: palette.darkGray, borderRadius: 2, padding: 2,}}
												/>
											)}
										</TouchableOpacity>
										<WalletContainer credits={user && user.customer? user.customer.credits : 0} wallet={0}/>
										<Categories handleComingSoon={this.handleComingSoon} />
									</>
								)}
							</View>
							<View style={dashboard.featured}>
								{/* <Deals /> */}
								<Restaurants />
							</View>
						</View>
					</ScrollView>
				</View>
			</>
		);
	}	

};


function mapStateToProps(state) {
	return {
	  user: state.user,
	  location: state.location,
	};
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
        authActions,
        dispatch,
    ),
  };
}
  
   
export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(Dashboard);