import React, { Component, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';

import {project} from '@config';

import routes from './routes';
import riderRoutes from './routes/rider';
const Stack = createStackNavigator();

class Router extends Component {
	constructor(props, context) {
		super(props, context);
		this.state = {
			isLoggedIn: false,
		};
	}
	render() {
		const {user} = this.props;
		return (
			<NavigationContainer style={styles.tabContainer}>
				<Stack.Navigator headerMode="none" initialRouteName={ user.token ? "Dashboard":"Landing"}>
					{project.build == 'rider' ?
						riderRoutes.map(route => (
							<Stack.Screen {...route} />
						))
						:
						routes.map(route => (
							<Stack.Screen {...route} />
						))
					}
				</Stack.Navigator>
			</NavigationContainer>
		);
	}
	
}

const styles = StyleSheet.create({
	tabContainer: {
		paddingVertical: 15,
		backgroundColor: '#000',
		height: 60,
		opacity: 1,
	},
	tabIcon: {
		marginTop: 15,
		height: 30,
		maxWidth: 30,
	},
	tabIcon2: {
		marginTop: 20,
		height: 40,
		maxWidth: 40,
	},
});

function mapStateToProps(state) {
	return {
	  user: state.user,
	};
}
  
   
export default connect(
	mapStateToProps,
	null,
)(Router);