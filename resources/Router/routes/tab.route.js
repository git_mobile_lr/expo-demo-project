import React from 'react';
import { Image } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Dashboard from 'modules/Dashboard/';
import Orders from 'modules/Orders';
import Wallet from 'modules/Wallet';
import Messages from 'modules/Messages';
import Account from 'modules/Account';

const Tab = createBottomTabNavigator();

const routes = [
    {key: "dashboard", name: "Dashboard", component: Dashboard, icon: require("/img/home.png")},
    {key: "orders", name: "Orders", component: Orders , icon: require("/img/orders.png")},
    // {key: "wallet", name: "Wallet", component: Wallet, icon: require("/img/wallet.png")},
    {key: "messages", name: "Messages", component: Messages, icon: require("/img/message.png")},
    {key: "account", name: "Account", component: Account, icon: require("/img/account.png")},
];

const DashboardRoute = () => {

    return (
        <Tab.Navigator initialRouteName="Dashboard"
            tabBarOptions={{
                showLabel: false,
                showIcon: true,
                safeAreaInsets: {
                    bottom: 10
                }
            }}
            barStyle={{ backgroundColor: 'white' }}>
                {
                    routes.map(route => (
                        <Tab.Screen
                            key={ route.key }
                            name={ route.name } 
                            component={route.component} 
                            options={{
                                tabBarIcon: ({ focused }) => (
                                    <Image
                                        resizeMode="contain"
                                        source={ route.icon }
                                        style={{
                                            width: 23,
                                            height: 23,
                                            opacity: focused ? 1 : 0.3
                                        }}
                                    />
                                ),
                            }}
                        />
                    ))
                }
        </Tab.Navigator>
    );
};

export default DashboardRoute;

