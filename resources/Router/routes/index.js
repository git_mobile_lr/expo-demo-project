import Dashboard from './tab.route';
import Food from 'modules/Food';
import Merchant from 'modules/Merchant/';
import Product from 'modules/Product/';
import About from 'modules/About/';
import OrderDetails from 'modules/Orders/Details/';
import Search from 'modules/Search/';
import Location from 'modules/Location/';
import MyCart from 'modules/MyCart/';
import MyCartMerchants from 'modules/MyCart/merchants'

// AUTH GROUP 
import Landing from 'modules/Authentication/Landing';
import Login from 'modules/Authentication/Login';
import Signup from 'modules/Authentication/Signup';

//CHECKOUT GROUP
import Checkout from 'modules/Checkout/';
import OneMoreStep from 'modules/OneMoreStep/';
import CheckoutLoading from 'modules/Checkout/loading';

export default routes = [
    {key: "landing", name: "Landing", component: Landing},
    {key: "login", name: "Login", component: Login},
    {key: "signup", name: "Signup", component: Signup},
    {key: "dashboard", name: "Dashboard", component: Dashboard, iconName: "home"},
    {key: "food", name: "Food", component: Food},
    {key: "merchant", name: "Merchant", component: Merchant},
    {key: "product", name: "Product", component: Product},
    {key: "about", name: "About", component: About},
    {key: "orderDetails", name: "OrderDetails", component: OrderDetails},
    {key: "search", name: "Search", component: Search},
    {key: "checkoutmerchants", name: "MyCartMerchants", component: MyCartMerchants},
    {key: "location", name: "Location", component: Location},
    {key: "mycart", name: "MyCart", component: MyCart},
    {key: "checkout", name: "Checkout", component: Checkout},
    {key: "onemorestep", name: "OneMoreStep", component: OneMoreStep},
    {key: "checkoutloading", name: "CheckoutLoading", component: CheckoutLoading}
];