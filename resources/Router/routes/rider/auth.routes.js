import Login from '@riderModules/auth/login';
import Register from '@riderModules/auth/register';

const authRoutes = [
  {key: 'login', name: 'Landing', component: Login},
  {key: 'register', name: 'Register', component: Register},
];

export default authRoutes;
