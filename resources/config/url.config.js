const URLs = {
  development: {
    base: '',
    api: '',
  },
  production: {
    base: '',
    api: '',
  },
};

const URL = URLs[process.env.NODE_ENV];

export { URL };
