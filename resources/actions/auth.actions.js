import {Auth, Network} from '../constants';
import * as API from '../helpers/API';
import { useDispatch } from 'react-redux';

export function checkServerConnection() {
  return function(dispatch) {
    dispatch({
      type: Network.CHECK_SERVER_CONNECTION,
    });
    return API.connect()
      .post('check-server-connection')
      .then(res => {
        dispatch({
          type: Network.CHECK_SERVER_CONNECTION_SUCCESS,
          payload: res,
        });
        return {
          type: Auth.CHECK_SERVER_CONNECTION_SUCCESS,
          payload: res,
        };
      })
      .catch(error => {
        dispatch({
          type: Network.CHECK_SERVER_CONNECTION_FAILED,
        });
        throw error;
      });
  };
}

export function signup(params) {
	return API.connect()
	.post('auth/register', params)
	.then(res => {
		return {
			type: Auth.SIGNUP_SUCCESS,
			payload: res
		};
	})
	.catch(error => {
		throw error;
	});

}

export function signupRider(params) {
	return API.connect()
	.post('auth/register-rider', params)
	.then(res => {
		return {
			type: Auth.SIGNUP_SUCCESS,
			payload: res
		};
	})
	.catch(error => {
		throw error;
	});

}

export function login(params) {
	return API.connect()
	.post('auth/authenticate', params)
	.then(res => {
		return {
			type: Auth.LOGIN_SUCCESS,
			payload: res,
		};
	})
	.catch(error => {
		throw error;
	});
}

export function logout() {
  return function(dispatch) {
    dispatch({
      type: Auth.LOGOUT,
    });
    return {
      type: Auth.LOGOUT,
      payload: {success: true},
    };
  };
}

export function getUserData(params = {}) {
  return function(dispatch) {
    dispatch({
      type: Auth.GET_USER_DATA,
    });
    return API.connect()
      .get('auth/get_user_details', {params})
      .then(res => {
        dispatch({
          type: Auth.GET_USER_DATA_SUCCESS,
          payload: res,
        });
        return {
          type: Auth.GET_USER_DATA_SUCCESS,
          payload: res,
        };
      })
      .catch(error => {
        dispatch({
          type: Auth.GET_USER_DATA_FAILED,
        });
        throw error;
      });
  };
}

export function updateFbData(params = {}) {
  return function(dispatch) {
    dispatch({
      type: Auth.UPDATE_FB_DATA,
      payload: params,
    });
  };
}

export function registerNumber(params) {
  return function(dispatch) {
    dispatch({
      type: Auth.REGISTER_NUMBER,
    });
    return API.connect()
      .post('auth/register_number', params)
      .then(res => {
        dispatch({
          type: Auth.REGISTER_NUMBER_SUCCESS,
          payload: {...res, ...{data: params}},
        });
        return {
          type: Auth.REGISTER_NUMBER_SUCCESS,
          payload: {...res, ...{data: params}},
        };
      })
      .catch(error => {
        dispatch({
          type: Auth.REGISTER_NUMBER_FAILED,
        });
        throw error;
      });
  };
}

export function smsVerify(params) {
  return function(dispatch) {
    dispatch({
      type: Auth.SMS_VERIFY,
    });
    return API.connect(true)
      .post('auth/sms_verify', params)
      .then(res => {
        dispatch({
          type: Auth.SMS_VERIFY_SUCCESS,
        });
        return {
          type: Auth.SMS_VERIFY_SUCCESS,
          payload: res,
        };
      })
      .catch(error => {
        dispatch({
          type: Auth.SMS_VERIFY_FAILED,
        });
        throw error;
      });
  };
}

export function FBLogin(params) {
  return function(dispatch) {
    dispatch({
      type: Auth.FB_LOGIN,
    });
    return API.connect()
      .post('auth/fb-login', params)
      .then(res => {
        dispatch({
          type: Auth.FB_LOGIN_SUCCESS,
          payload: res,
        });
        return {
          type: Auth.FB_LOGIN_SUCCESS,
          payload: res,
        };
      })
      .catch(error => {
        dispatch({
          type: Auth.FB_LOGIN_FAILED,
        });
        throw error;
      });
  };
}

export function addDeviceToken(params) {
  return function(dispatch) {
    dispatch({
      type: Auth.ADD_DEVICE_TOKEN,
    });
    return API.connect()
      .post('user_devices/store', params)
      .then(res => {
        dispatch({
          type: Auth.ADD_DEVICE_TOKEN_SUCCESS,
          payload: res,
        });
        return {
          type: Auth.ADD_DEVICE_TOKEN_SUCCESS,
          payload: res,
        };
      })
      .catch(error => {
        dispatch({
          type: Auth.ADD_DEVICE_TOKEN_FAILED,
        });
        throw error;
      });
  };
}

export function removeDeviceToken(params) {
  return function(dispatch) {
    dispatch({
      type: Auth.REMOVE_DEVICE_TOKEN,
    });
    return API.connect()
      .post('user_devices/destroy', params)
      .then(res => {
        dispatch({
          type: Auth.REMOVE_DEVICE_TOKEN_SUCCESS,
          payload: res,
        });
        return {
          type: Auth.REMOVE_DEVICE_TOKEN_SUCCESS,
          payload: res,
        };
      })
      .catch(error => {
        dispatch({
          type: Auth.REMOVE_DEVICE_TOKEN_FAILED,
        });
        throw error;
      });
  };
}
