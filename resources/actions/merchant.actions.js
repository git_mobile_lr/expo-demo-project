import * as API from '../helpers/API';

export function getMerchants() {
  return API.connect().get('/stores/get')
}

export function findProduct(slug) {
  return API.connect().get(`/products/find?slug=${ slug }`)
}

export function findMerchant(id) {
  return API.connect().get(`/stores/find?id=${ id }`)
}

export function getCategories(id) {
  return API.connect().get(`/categories/all?store_id=${ id }`)
}

export function getMerchantProducts(id) {
  return API.connect().get(`/v2/products/caboodle_get?store_id=${ id }`)
}

export function getMerchantProductsByCategory(categoryId, page = 0) {
  return API.connect().get(`/products/get_by_category?category=${ categoryId }&page=${ page }`)
}