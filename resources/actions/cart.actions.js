// import {Auth} from '../constants';
import * as API from '../helpers/API';
import {store} from 'store/configureStore';
import { Cart } from 'constants'

export function updateCartMerchant(cartMerchant) {
    store.dispatch({
        type: Cart.UPDATE_CART_MERCHANT,
        payload: cartMerchant
    })
}

export function getCart() {
    return API.connect().get('/orders/user/cart')
}

export function syncCart( slug) {
    return API.connect().post('/orders/sync-cart')
}

export function checkout(id) {
    return API.connect().post('/orders/checkout')
}

export function getHistory(id) {
    return API.connect().get('/orders/history')
}

export function getMerchantProducts(id) {
  return API.connect().get(`/v2/products/caboodle_get?store_id=${ id }`)
}

export function getMerchantProductsByCategory(categoryId, page = 0) {
  return API.connect().get(`/products/get_by_category?category=${ categoryId }&page=${ page }`)
}