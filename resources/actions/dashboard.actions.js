import {Dashboard, Options} from '../constants';
import * as API from '../helpers/API';

export function switchDashboardType(type = null) {
  return {
    type: Dashboard.SWITCH_DASHBOARD_TYPE,
    payload: {type},
  };
}

export function getServiceCategories() {
  const params = {append_services: 1};
  return function(dispatch) {
    dispatch({
      type: Dashboard.GET_SERVICE_CATEGORIES,
    });
    return API.connect()
      .get('categories/get', {params})
      .then(res => {
        dispatch({
          type: Dashboard.GET_SERVICE_CATEGORIES_SUCCESS,
          payload: res,
        });
        return {
          type: Dashboard.GET_SERVICE_CATEGORIES_SUCCESS,
          payload: res,
        };
      })
      .catch(error => {
        dispatch({
          type: Dashboard.GET_SERVICE_CATEGORIES_FAILED,
        });
        throw error;
      });
  };
}

export function getCountries() {
  return function(dispatch) {
    dispatch({
      type: Dashboard.GET_COUNTRIES,
    });
    return API.connect()
      .get('countries/get')
      .then(res => {
        dispatch({
          type: Dashboard.GET_COUNTRIES_SUCCESS,
          payload: res,
        });
        return {
          type: Dashboard.GET_COUNTRIES_SUCCESS,
          payload: res,
        };
      })
      .catch(error => {
        dispatch({
          type: Dashboard.GET_COUNTRIES_FAILED,
        });
        throw error;
      });
  };
}

export function getOptions() {
  return function(dispatch) {
    dispatch({
      type: Options.GET_OPTIONS,
    });
    return API.connect()
      .get('options/get_all')
      .then(res => {
        dispatch({
          type: Options.GET_OPTIONS_SUCCESS,
          payload: res,
        });
        return {
          type: Options.GET_OPTIONS_SUCCESS,
          payload: res,
        };
      })
      .catch(error => {
        dispatch({
          type: Options.GET_OPTIONS_FAILED,
        });
        throw error;
      });
  };
}
