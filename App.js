/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React, {useEffect} from 'react';
 import {Provider} from 'react-redux';
 import {PersistGate} from 'redux-persist/integration/react';
 import {store, persistor} from './resources/store/configureStore';
 import {
   StatusBar,
 } from 'react-native';
 import LinearGradient from 'react-native-linear-gradient';
 import SplashScreen from 'react-native-splash-screen';
 import { useFonts } from 'expo-font';
 import AppLoading from 'expo-app-loading';

//  import messaging from '@react-native-firebase/messaging';
 
 import DashboardHeader from './resources/components/DashboardHeader';
 import palette from './resources/styles/palette.styles';
 import Router from './resources/Router/index';
 
 import MessagePopup, {
   MessagePopupRef,
 } from './resources/components/common/MessagePopup';
 import LoadingOverlay, {
   LoadingOverlayRef,
 } from './resources/components/common/LoadingOverlay';
//  import Firebase from './resources/components/common/Firebase';
 
 import {project} from '@config';
 
 import Camera from '@components/camera';
 
 // Register background notification handler // TODO FIX THIS
//  messaging().setBackgroundMessageHandler(async remoteMessage => {
//    console.log('Message handled in the background!', remoteMessage);
//  });
 
 const App = () => {
   useEffect(() => {
    //  SplashScreen.hide();
   }, []);


   let [fontsLoaded] = useFonts({
    'SFProText-Bold': require('./resources/fonts/SFProText-Bold.ttf'),
    'SFProText-Light': require('./resources/fonts/SFProText-Light.ttf'),
    'SFProText-Medium': require('./resources/fonts/SFProText-Medium.ttf'),
    'SFProText-Regular': require('./resources/fonts/SFProText-Regular.ttf'),
    'SFProText-Semibold': require('./resources/fonts/SFProText-Semibold.ttf'),
  });
   
  if (!fontsLoaded) {
    return (<AppLoading />);
  } else {
   return (
     <Provider store={store}>
       <PersistGate loading={null} persistor={persistor}>
       <>
         <StatusBar barStyle="dark-content" backgroundColor={palette.white} />
         {/* <Firebase /> */}
         <Router />
         <MessagePopup ref={MessagePopupRef} />
         <LoadingOverlay ref={LoadingOverlayRef} />
         <Camera />
       </>
       </PersistGate>
     </Provider>
   );
  }
 };
 
 
 export default App;
 